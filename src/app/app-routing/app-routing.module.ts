import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AppComponent } from '../components/app.component';
import { HomeComponent } from '../components/home/home.component';
import { LoginComponent } from '../components/login/login.component';
import { BoardComponent } from '../components/board/board.component';
import { Routes, RouterModule } from '@angular/router';

const routes : Routes = [
	{path: '', component: AppComponent},
	{path: 'register', component: HomeComponent},
	{path: 'login', component: LoginComponent},
	{path: 'board/:salario', component: BoardComponent},
	{path: '**', component: AppComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  declarations: []
})
export class AppRoutingModule { }
