import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Router } from '@angular/router';
import { GLOBAL } from './global';

interface Options {
	command?: string;
	salario?: number;
	error?: string;
};
//Decorador

@Injectable()
export class UserService{
	public url: string;
	public identity;

	constructor(
		private http: HttpClient,
		private _router: Router,
	){
		this.url = GLOBAL.url;
	}

	signup(user_to_signup){
		const body = { ...user_to_signup };
		let headers =	{ 'Content-Type':'application/json' };
		let response = {};
		this.http.post(this.url+'register', body, { headers })
			.subscribe((data: Options) => {
				console.log(data);
				if (data.command == 'INSERT') {
					response = data;
					alert('El usuario se ha registrado exitosamente');
					this._router.navigate(['/register']);
				} if (data.error) {
					alert('ups parece que ocurrio algo.. intentalo de nuevo');
				} else {
					alert('ups!. El usuario no se registro correctamente, verifique la informacion');
				}
		});
		return response;
	}

	login(user_to_login){
		const body = { ...user_to_login };
		let headers =	{ 'Content-Type':'application/json' };
		return this.http.post(this.url+'login', body, { headers })
			.subscribe((data: Options) => {
				if (data.salario) {
					alert('El usuario se ha logueado correctamente');
					this._router.navigate(['/board', data.salario]);
				} else {
					alert('usuario o correo invlido');
				}
			});
	}
}