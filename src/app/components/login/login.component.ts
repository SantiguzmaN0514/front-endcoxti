import { Component } from '@angular/core';
import { UserService } from '../../services/user.service';
import { User } from '../../models/user';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers: [UserService]
})
export class LoginComponent {
  public user: User;
  tituloAlerta:string = '';

  constructor(private _userService: UserService) {
    this.user = new User(0, '', 0, '', '', '', '', '', '', 0, 0, 0, 0);
  }

  login(){
    this.user.correo = this.user.correo;
    this.user.password = this.user.password;
    this._userService.login(this.user);
  }
}
