import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-board',
  templateUrl: './board.component.html',
  styleUrls: ['./board.component.css']
})
export class BoardComponent implements OnInit {
  public salario;
  public optionRes = [];
  public seleccion;
  public valCorrecto;

  constructor(private route: ActivatedRoute) { }

  ngOnInit() {
    let salario = parseInt(this.route.snapshot.paramMap.get("salario"));
    this.optionRes = [];
    const uno = Math.floor(Math.random() * salario/2);
    const dos = Math.floor(Math.floor(Math.random() * salario*2.5 ) + salario*2);
    this.optionRes.push(`entre ${Math.floor(uno * 0.8)} y ${Math.floor(uno * 1.2)}`);
    this.optionRes.push(`entre ${Math.floor(salario * 0.8)} y ${Math.floor(salario * 1.2)}`);
    this.optionRes.push(`entre ${Math.floor(dos * 0.8)} y ${Math.floor(dos * 1.2)}`);
    this.valCorrecto = this.optionRes[1];
    this.optionRes.sort();
  }

  verificar(){
    if (this.seleccion === this.valCorrecto) {
      alert('La seleccion es correcta');
    } else {
      alert('La seleccion es incorrecta');
    }
  }
}
