import { Component } from '@angular/core';
import { User } from '../../models/user';
import { Router, ActivatedRoute } from '@angular/router';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  providers: [UserService]
})

export class HomeComponent {
  public user: User;
  public status: string;

  constructor(
    private _route: ActivatedRoute,
    private _router: Router,
    private _userService: UserService
  ) {
    this.user = new User(null, '', null, '', '', '', '', '', '', null, null, null, null);
  }

  validarData(){
    this.user.id=this.user.id;
    this.user.nombre=this.user.nombre;
    this.user.celular =  this.user.celular;
    this.user.correo= this.user.correo;
    this.user.password= this.user.password;
    this.user.departamento= this.user.departamento;
    this.user.ciudad= this.user.ciudad;
    this.user.barrio= this.user.barrio;
    this.user.direccion= this.user.direccion;
    this.user.salario= this.user.salario;
    this.user.otrosingresos= this.user.otrosingresos;
    this.user.gastosmensuales= this.user.gastosmensuales;
    this.user.gastosfinancieros= this.user.gastosfinancieros;
    this._userService.signup(this.user);
    this.user = new User(null, '', null, '', '', '', '', '', '', null, null, null, null);
  }
}
