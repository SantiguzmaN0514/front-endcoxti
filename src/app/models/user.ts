export class User {
  constructor(
    public id: number,
    public nombre: string,
    public celular: number,
    public correo: string,
    public password: string,
    public departamento: string,
    public ciudad: string,
    public barrio: string,
    public direccion: string,
    public salario: number,
    public otrosingresos: number,
    public gastosmensuales: number,
    public gastosfinancieros: number
  ){}
};
